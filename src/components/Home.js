import React from 'react';
import PropTypes from 'prop-types';

const Home = (props) => {
  const { 
      count, 
      increment, 
      isIncrementing, 
      incrementAsync, 
      decrement,
      isDecrementing,
      decrementAsync,
      changePage 
    } = props;

  return (
    <div>
        <h1>Home</h1>
        <p>Count: {count}</p>

        <p>
        <button onClick={increment} disabled={isIncrementing}>
            Increment
        </button>
        <button onClick={incrementAsync} disabled={isIncrementing}>
            Increment Async
        </button>
        </p>

        <p>
        <button onClick={decrement} disabled={isDecrementing}>
            Decrement
        </button>
        <button onClick={decrementAsync} disabled={isDecrementing}>
            Decrement Async
        </button>
        </p>

        <p>
        <button onClick={() => changePage()}>
            Go to about page via redux
        </button>
        </p>

    </div>
  );
};

Home.propTypes = {
    count: PropTypes.number,
    increment: PropTypes.func,
    isIncrementing: PropTypes.bool,
    incrementAsync: PropTypes.func,
    decrement: PropTypes.func,
    isDecrementing: PropTypes.bool,
    decrementAsync: PropTypes.func,
    changePage: PropTypes.func,
};

export default Home;