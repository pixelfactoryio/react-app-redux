import loggerMiddleware from './middleware/loggerMiddleware';
import { routerMiddleware } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';
import thunk from 'redux-thunk';

export const history = createHistory();

const middleware = [
  loggerMiddleware,
  thunk,
  routerMiddleware(history),
];

export default middleware